<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WebAuthController@index')->name('login-page');
Route::post('/login', 'WebAuthController@login')->name('login');
Route::get('/logout','WebAuthController@logout')->name('logout');
Route::post('/register','WebAuthController@register')->name('register');

Route::get('/profilesetup','WebAuthController@profileSetup')->name('profile-setup');
Route::post('/save-profilesetup','WebAuthController@saveUserInfo')->name('save-profile-setup');

Route::get('/contactsetup','WebAuthController@contactSetup')->name('contact-setup');


Route::get('/dashboard', function () {
    return view('page-template');
});

