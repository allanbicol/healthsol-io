@extends('auth-template')

@section('title')
    <title>Healthsol - Profile Setup</title>
@endsection

@section('content')
    <div class="landing-page-setup">
        <canvas id="pixie"></canvas>
        <div class="profile-setup-section">
            <div class="row no-gutters">
                <div class="col-lg-6 offset-lg-1 col-md-6 offset-md-1">
                    <div class="setup-form-container">
                        <div class="form-panel">
                            <div class="pagination-setup">
                                <!-- design process steps-->
                                <h2 class="text-center">Profile Setup</h2>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                                    <li role="presentation" class="done">
                                        <p><i class="fas fa-check"></i>
                                            <p class="tab-title">REGISTER / LOGIN</p>
                                        </p>
                                    </li>
                                    <li role="presentation" class="current">
                                        <p><i class="fas fa-circle"></i>
                                            <p class="tab-title">PROFILE INFO</p>
                                        </p>
                                    </li>
                                    <li role="presentation" class="normal">
                                        <p><i class="fas fa-circle"></i>
                                            <p class="tab-title">CONTACT INFO</p>
                                        </p>
                                    </li>
                                </ul>
                                <!-- end design process steps-->

                            </div>
                            <div class="form-container">
                                <form method="POST" action="{{route('save-profile-setup')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_id" value="{{$user_id}}"/>
                                    <input type="hidden" name="username" value="{{$username}}"/>
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label for="firstname_user">First Name</label>
                                                <input type="text" name="firstname" class="form-control" id="firstname_user" placeholder="First Name" value="" required="required">
                                            </div>
                                            <div class="form-group">
                                                <label for="middlename_user">Middle Name</label>
                                                <input type="text" name="middlename" class="form-control" id="middlename_user" value="" placeholder="Middle Name" required="required">
                                            </div>
                                            <div class="form-group">
                                                <label for="lastname_user">Last Name</label>
                                                <input type="text" name="lastname" class="form-control" id="lastname_user" value="" placeholder="Last Name" required="required">
                                            </div>
                                            <div class="form-group">
                                                <label for="gender_select">Gender</label>
                                                <select class="form-control" id="gender_select" name="gender">
                                                    <option value="">Please Select Gender</option>
                                                    <option value="0">Male</option>
                                                    <option value-="1">Female</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="gender_select">Civil Status</label>
                                                <select class="form-control" id="civil_select" name="civilstatus">
                                                    <option value="">Please Select Status</option>
                                                    @foreach ($civil_status as $status)
                                                        <option value="{{$status->id}}">{{$status->civil_status}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="gender_select">Country</label>
                                                <select class="form-control" id="gender_select" name="ethnicity_country">
                                                    <option value="">Please Select Country</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label for="ethnicity_user">Date of Birth</label>
                                                <input type="date" class="form-control" id="ethnicity_user" name="dob" value="0000-00-00" placeholder="Date of Birth">
                                            </div>
                                            <div class="form-group">
                                                <label for="bof_user">Birth of Place</label>
                                                <input type="text" class="form-control" id="bof_user" name="pob" value="" placeholder="Place of Birth">
                                            </div>


                                            <div class="form-group">
                                                <label for="height_user">Height</label>
                                                <input type="number" class="form-control" id="height_user" name="height" placeholder="Height">
                                            </div>
                                            <div class="form-group">
                                                <label for="weight_user">Weight</label>
                                                <input type="number" class="form-control" id="weight_user" name="weight" placeholder="Weight">
                                            </div>
                                            <div class="form-group">
                                                <label for="mobile_user">Blood Type</label>
                                                <select class="form-control" id="blood_select" name="bloodtype">
                                                    <option value="">Please Select Blood Type</option>
                                                    @foreach ($blood_types as $blood_type)
                                                        <option value="{{$blood_type->blood_type_id}}">{{$blood_type->blood_type_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="profile-info-submit">
                                            <button class='btn btn-primary' type="submit">CONTINUE</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
