@extends('auth-template')

@section('title')
    <title>Healthsol - Auth</title>
@endsection

@section('content')
    <div class="landing-page">
        <canvas id="pixie"></canvas>
        <div class="login-section">
            <div class="img-logo-container">
                <div class="img-logo">
                    <img class="img-fluid" src="{{ URL::asset('front-assets/img/login/healthsol-portrait.png')}}" alt="">
                </div>
            </div>
            <ul class="nav nav-pills landing-page-nav-tab mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link {{ session()->get('tab') === 'login-tab' ? 'active' : '' }}" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="{{ session()->get('tab') === 'login-tab' ? 'true' : 'false' }}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ session()->get('tab') === 'register-tab' ? 'active' : '' }}" id="pills-signup-tab" data-toggle="pill" href="#pills-signup" role="tab" aria-controls="pills-signup" aria-selected="{{ session()->get('tab') === 'register-tab' ? 'true' : 'false' }}">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-forgot-tab" data-toggle="pill" href="#pills-forgot" role="tab" aria-controls="pills-forgot" aria-selected="false">Forgot Password?</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
                @if(isset(Auth::user()->email))
                    <script> window.location = "/dashboard";</script>
                @endif

                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </u>

                    </div>
                @endif
                @if($message = Session::get('signup-success'))
                    <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {{ $message }}.
                    </div>
                @endif


                <div class="tab-pane fade {{ session()->get('tab') === 'login-tab' ? ' show active' : '' }}" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                    <div class="row no-gutters">
                        <form method="POST" action="{{ route('login')}}">
                            {{ csrf_field() }}
                            <div class="form-group ">
                                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" required>
                                <div class="alert-info-container">
                                    <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="<p class='asasa'>Please fill in this field</p>"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>
                            <div class="form-group input-error">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                <div class="alert-info-container">
                                    <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="<p class='asasa'>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>
                            <div class="form-group custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitch2">
                                <label class="custom-control-label" for="customSwitch2">Remember Me</label>
                            </div>
                            <div class="login-submit">
                                <button class='btn btn-primary btn-block' type="submit">LOGIN</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade {{ session()->get('tab') === 'register-tab' ? 'show active' : '' }}" id="pills-signup" role="tabpanel" aria-labelledby="pills-signup-tab">
                    <div class="row no-gutters">
                        <form method="POST" action="{{ route('register')}}">
                                {{ csrf_field() }}
                            <div class="form-group  @if($message = Session::get('errorusername')) input-error @endif">
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                                <div class="alert-info-container">
                                <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="@if($message = Session::get('errorusername')) <p class='asasa'>{{$message}}</p> @endif"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Email Address">
                                <div class="alert-info-container">
                                    <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="<p class='asasa'>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                <div class="alert-info-container">
                                    <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="<p class='asasa'>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                                <div class="alert-info-container">
                                    <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="<p class='asasa'>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>
                            <div class="form-group custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitch1" required>
                                <label class="custom-control-label" for="customSwitch1">I accept the policy and terms</label>
                            </div>
                            <div class="signup-submit">
                                <button class='btn btn-primary btn-block' type="submit">SIGN UP</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-forgot" role="tabpanel" aria-labelledby="pills-forgot-tab">
                    <div class="row no-gutters">
                        <form>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email">
                                <div class="alert-info-container">
                                    <span class="email-tooltip" data-toggle="tooltip" data-placement="right" data-html="true" title="<p class='asasa'>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>"><i class="fas fa-info-circle"></i></span>
                                </div>
                            </div>

                            <div class="reset-submit">
                                <button class='btn btn-primary btn-block' type="submit">RESET PASSWORD</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

