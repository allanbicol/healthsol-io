@extends('auth-template')

@section('title')
    <title>Healthsol - Contact Setup</title>
@endsection

@section('content')
    <div class="landing-page-setup">
        <canvas id="pixie"></canvas>
        <div class="profile-setup-section">
            <div class="row no-gutters">
                <div class="col-lg-6 offset-lg-1 col-md-6 offset-md-1">
                    <div class="setup-form-container">
                        <div class="form-panel">
                            <div class="pagination-setup">
                                <!-- design process steps-->
                                <h2 class="text-center">Profile Setup</h2>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                                    <li role="presentation" class="done">
                                        <p><i class="fas fa-check"></i>
                                            <p class="tab-title">REGISTER / LOGIN</p>
                                        </p>
                                    </li>
                                    <li role="presentation" class="done">
                                        <p><i class="fas fa-check"></i>
                                            <p class="tab-title">PROFILE INFO</p>
                                        </p>
                                    </li>
                                    <li role="presentation" class="current">
                                        <p><i class="fas fa-circle"></i>
                                            <p class="tab-title">CONTACT INFO</p>
                                        </p>
                                    </li>
                                </ul>
                                <!-- end design process steps-->

                            </div>
                            <div class="form-container">
                                <form>
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label for="civilstatus_select">Civil Status</label>
                                                <select class="form-control" id="civilstatus_select">
                                                    <option>Single</option>
                                                    <option>Married</option>
                                                    <option>Separated</option>
                                                    <option>Widowed</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="middlename_user">Country</label>
                                                <input type="text" class="form-control" id="middlename_user" placeholder="iddle NameM">
                                            </div>
                                            <div class="form-group">
                                                <label for="address1_user">Address (Block , Lot , Street)</label>
                                                <input type="text" class="form-control" id="address1_user" placeholder="Address">
                                            </div>
                                            <div class="form-group">
                                                <label for="address2_user">Address (Barangay , City , Province)</label>
                                                <input type="text" class="form-control" id="address2_user" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label for="zipcode_user">Zipcode</label>
                                                <input type="text" class="form-control" id="zipcode_user" placeholder="Zipcode">
                                            </div>
                                            <div class="form-group">
                                                <label for="country_user">Mobile Country Code</label>
                                                <input type="text" class="form-control" id="country_user" placeholder="Country">
                                            </div>
                                            <div class="form-group">
                                                <label for="mobilecode_user">Mobile Code</label>
                                                <input type="text" class="form-control" id="mobilecode_user" placeholder="+63">
                                            </div>
                                        </div>
                                        <div class="profile-info-submit">
                                            <button class='btn btn-primary' type="submit">CONTINUE</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
