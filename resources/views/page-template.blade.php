<!DOCTYPE HTML>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('front-assets/frameworks/bootstrap-4.3.1-dist/css/bootstrap.min.css')}}">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ URL::asset('front-assets/frameworks/fontawesome-free-5.8.1-web/css/all.min.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ URL::asset('front-assets/css/main.css')}}">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    @yield('title')
</head>

<body class="d-flex flex-column">
    <div class="dashboard-page">
        <div class="row no-gutters">
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3">
                <div class="sidebar sticky-top flex-grow-1">
                    <div class="brand-logo-container">
                        <img class="img-fluid" src="{{ URL::asset('front-assets/img/healthsol_caps.png')}}" alt="">
                    </div>
                    <ul class="nav flex-column dashboard-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25.598" height="24" viewBox="0 0 25.598 24"><path class="dashboard-icon" d="M12.8,2A12.8,12.8,0,0,1,19,26H6.6A12.8,12.8,0,0,1,12.8,2Zm7.248,20.048a10.183,10.183,0,0,0,3-7.248H20.8V13.2h2.126A10.127,10.127,0,0,0,21.862,10H19.2V8.4h1.607a10.379,10.379,0,0,0-.759-.848A10.21,10.21,0,0,0,16,5.057V6.8H14.4V4.673a10.385,10.385,0,0,0-3.2,0V6.8H9.6V5.057a10.2,10.2,0,0,0-4.048,2.5,10.379,10.379,0,0,0-.759.848H6.4V10H3.739a10.127,10.127,0,0,0-1.064,3.2H4.8v1.6H2.55a10.184,10.184,0,0,0,3,7.248,10.306,10.306,0,0,0,.839.752H11.2L12.115,10h1.371L14.4,22.8h4.809a10.456,10.456,0,0,0,.839-.752Z" transform="translate(-0.002 -2)" fill="#00a8ff"/></svg>
                                </span> Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.902" height="17.787" viewBox="0 0 24.902 17.787"><path class='message-icon' d="M423,1346v17.787h24.9V1346Zm21.345,1.779-8.894,7.293-8.894-7.293Zm-19.566,1.779,7.026,5.336-7.026,5.336Zm1.779,12.451,7.026-5.692,1.868,1.423,1.868-1.423,7.026,5.692Zm19.566-1.779-7.026-5.336,7.026-5.336Z" transform="translate(-423 -1346)" fill="#636363"/></svg>
                                </span> Message
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21.006" height="21.006" viewBox="0 0 21.006 21.006"><path class="appointment-icon" d="M21.006,21.006H0V2.626H2.626V18.38H18.38V2.626h2.626ZM3.939,7.877H6.564V10.5H3.939Zm5.252,0h2.626V10.5H9.19Zm5.252,0h2.626V10.5H14.442Zm-10.5,5.252H6.564v2.626H3.939Zm5.252,0h2.626v2.626H9.19Zm5.252,2.626V13.129h2.626ZM5.252,3.939V1.313a1.313,1.313,0,0,1,2.626,0V3.939a1.313,1.313,0,0,1-2.626,0Zm7.877,0V1.313a1.313,1.313,0,1,1,2.626,0V3.939a1.313,1.313,0,1,1-2.626,0Z" fill="#636363"/></svg>
                                </span> Calendar
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.008" height="21.006" viewBox="0 0 24.008 21.006"><path class="transaction-icon" d="M9,8H6V6.5A1.5,1.5,0,0,1,7.5,5h9a1.5,1.5,0,0,1,1.5,1.5V8h3V6.5A4.506,4.506,0,0,0,16.5,2h-9A4.506,4.506,0,0,0,3,6.5V8H0l4.5,6Zm10.5,3L15,17h3v1.5a1.5,1.5,0,0,1-1.5,1.5h-9A1.5,1.5,0,0,1,6,18.5V17H3v1.5a4.506,4.506,0,0,0,4.5,4.5h9a4.506,4.506,0,0,0,4.5-4.5V17h3Z" transform="translate(0 -2)" fill="#636363"/></svg>
                                </span> Activities
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21.436" height="24.884" viewBox="0 0 21.436 24.884"><path class="settings-icon" d="M14.65,15.181l.488-.488a1.381,1.381,0,0,0,0-1.953l-.843-.842a6.532,6.532,0,0,0,.632-1.517h.912A1.381,1.381,0,0,0,17.22,9v-.69a1.381,1.381,0,0,0-1.381-1.381h-.9a6.514,6.514,0,0,0-.611-1.506l.811-.81a1.381,1.381,0,0,0,0-1.953L14.65,2.17a1.381,1.381,0,0,0-1.953,0l-.794.794a6.512,6.512,0,0,0-1.565-.655V1.381A1.382,1.382,0,0,0,8.955,0h-.69A1.381,1.381,0,0,0,6.883,1.381v.928a6.524,6.524,0,0,0-1.565.655l-.795-.795a1.381,1.381,0,0,0-1.953,0l-.488.488a1.381,1.381,0,0,0,0,1.953l.81.811a6.507,6.507,0,0,0-.611,1.506h-.9A1.38,1.38,0,0,0,0,8.308V9A1.381,1.381,0,0,0,1.381,10.38h.911A6.534,6.534,0,0,0,2.925,11.9l-.843.842a1.381,1.381,0,0,0,0,1.953l.488.488a1.381,1.381,0,0,0,1.953,0l.848-.848a6.512,6.512,0,0,0,1.511.622v.884a1.382,1.382,0,0,0,1.382,1.381h.69a1.381,1.381,0,0,0,1.381-1.381v-.884a6.518,6.518,0,0,0,1.511-.622l.848.848a1.384,1.384,0,0,0,1.955,0ZM8.61,12.472a3.8,3.8,0,1,1,3.8-3.8A3.8,3.8,0,0,1,8.61,12.472Zm0-5.567a1.726,1.726,0,1,0,1.726,1.726A1.726,1.726,0,0,0,8.61,6.905ZM17.263,19.88a.837.837,0,1,0,.838.838A.837.837,0,0,0,17.263,19.88Zm3.506.01h-.437a3.184,3.184,0,0,0-.3-.731l.393-.393a.669.669,0,0,0,0-.947l-.237-.237a.669.669,0,0,0-.947,0l-.386.385a3.182,3.182,0,0,0-.759-.318V17.2a.671.671,0,0,0-.67-.67h-.335a.671.671,0,0,0-.67.67v.449a3.17,3.17,0,0,0-.759.318l-.386-.385a.669.669,0,0,0-.947,0l-.237.237a.669.669,0,0,0,0,.947l.393.393a3.141,3.141,0,0,0-.3.731h-.437a.671.671,0,0,0-.67.67V20.9a.671.671,0,0,0,.67.67H14.2a3.174,3.174,0,0,0,.307.736l-.408.408a.67.67,0,0,0,0,.948l.237.237a.671.671,0,0,0,.947,0l.411-.412a3.17,3.17,0,0,0,.733.3v.429a.671.671,0,0,0,.67.67h.335a.671.671,0,0,0,.67-.67v-.429a3.123,3.123,0,0,0,.732-.3l.412.412a.671.671,0,0,0,.947,0l.237-.237a.67.67,0,0,0,0-.948l-.408-.408a3.2,3.2,0,0,0,.307-.736h.442a.671.671,0,0,0,.67-.67v-.335a.666.666,0,0,0-.667-.67Zm-3.506,2.69A1.842,1.842,0,1,1,19.1,20.738,1.842,1.842,0,0,1,17.263,22.581Z" fill="#636363"/></svg>
                                </span> Settings
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-10 col-md-10">
                <div class="header-dashboard-content">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <form class="form-inline search-form">
                            <span class="search-icon"><i class="fas fa-search"></i></span>
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        </form>
                        <div class="ml-auto">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <div class="notification-center-container dropdown">
                                        <a class="notification-btn dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bell"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-right animated fadeInUp" aria-labelledby="dropdownMenuButton">
                                            <div class="notification-center-header">
                                                <div class="row no-gutters">
                                                    <div class="col-8">
                                                        <p class="notification-head-title">
                                                            Notification
                                                        </p>
                                                    </div>
                                                    <div class="col-4 link-all">
                                                        <a href="#" class="btn btn-link">Clear All</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <ul class="notification-list">
                                                <li class="notification-item request-notif">
                                                    <div class="notification-item-container">
                                                        <div class="row no-gutters">
                                                            <div class="col-2">
                                                                <div class="user-img">

                                                                </div>
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="user-name">
                                                                    John Doe
                                                                </div>
                                                                <div class="notification-text">
                                                                    Sent you a Consultation Request.
                                                                </div>
                                                                <div class="notification-action">
                                                                    <a href='#' class="btn btn-cancel btn-light" data-dismiss="modal">DECLINE</a>
                                                                    <a href='#' class="btn btn-send btn-light" data-dismiss="modal">CONFIRM</a>
                                                                </div>
                                                                <div class="notification-time">
                                                                    2 minutes ago
                                                                </div>
                                                            </div>
                                                            <div class="col-1">
                                                                <div class="close-action">
                                                                    <a href="#" class="btn btn-close-notif"><i class="fas fa-times"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="notification-item admin-notif">
                                                    <div class="notification-item-container">
                                                        <div class="row no-gutters">
                                                            <div class="col-2">
                                                                <div class="user-img">

                                                                </div>
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="user-name">
                                                                    John Doe
                                                                </div>
                                                                <div class="notification-text">
                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                                                                </div>
                                                                <div class="notification-time">
                                                                    2 hours ago
                                                                </div>
                                                            </div>
                                                            <div class="col-1">
                                                                <div class="close-action">
                                                                    <a href="#" class="btn btn-close-notif"><i class="fas fa-times"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="notification-item sched-notif">
                                                    <div class="notification-item-container">
                                                        <div class="row no-gutters">
                                                            <div class="col-2">
                                                                <div class="user-img">

                                                                </div>
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="user-name">
                                                                    John Doe
                                                                </div>
                                                                <div class="notification-text">
                                                                    You have appointment to <span>Dr.Fuji</span> on <span>April 20, 2019, 2 : 00 PM</span> at <span>Medical Plaza Ortigas</span>
                                                                </div>
                                                                <div class="notification-action">
                                                                    <a href='#' class="btn btn-view btn-light btn-block" data-dismiss="modal">VIEW SCHEDULE</a>
                                                                </div>
                                                                <div class="notification-time">
                                                                    2 minutes ago
                                                                </div>
                                                            </div>
                                                            <div class="col-1">
                                                                <div class="close-action">
                                                                    <a href="#" class="btn btn-close-notif"><i class="fas fa-times"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <div class="user-menu-container dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img class="img-fluid" src="{{ URL::asset('front-assets/img/dashboard/avatar.png')}}" alt=""><span>John Doe</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-right animated fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item" href="#">View Profile Info</a>
                                            <a class="dropdown-item" href="#">Sign Out</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="dashboard-content">
                    <div class="main-content-container">
                        <div class="row no-gutters">
                            <!-- main content -->

                            <div class="col-lg-10 col-md-10">

                                @yield('content')

                                <div class="transaction-nav-tab-container">
                                    <div class="transaction-nav-tab">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <!-- <ul class="transaction-tab-menu">
                                                    <li class="active">
                                                        ALL
                                                    </li>
                                                    <li>
                                                        PROFILE
                                                    </li>
                                                    <li>
                                                        CONSULTATION
                                                    </li>
                                                </ul> -->
                                                <ul class="nav nav-pills transaction-tab-menu" id="pills-tab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-home" aria-selected="true">ALL</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">PROFILE</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-consultation-tab" data-toggle="pill" href="#pills-consultation" role="tab" aria-controls="pills-contact" aria-selected="false">CONSULTATION</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="transaction-timeline-container">
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                                            <div class="transaction-timeline">
                                                <ul>
                                                    <li class="transaction-item">
                                                        <p class="timeline-date">Today</p>
                                                        <div class="transaction-card-container">
                                                            <div class="tag-status done"></div>
                                                            <div class="user-details-container">
                                                                <div class="avatar-container">
                                                                    <img src="img/dashboard/avatar.png" alt="">
                                                                </div>
                                                                <div class="trans-info-prof">
                                                                    <p class="user-name">John Doe</p>
                                                                    <p class="timestamp">10 minutes ago</p>
                                                                </div>
                                                            </div>
                                                            <div class="transaction-card-content">
                                                                <div class="transaction-type-container">
                                                                    <div class="trans-icon">
                                                                        <img src="img/dashboard/status/user.png" alt="">
                                                                    </div>
                                                                    <span>Profile Updated</span>
                                                                </div>
                                                                <div class="transaction-content">
                                                                    <div class="profile-changes">
                                                                        <table class="table table-borderless">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th scope="col">Changes</th>
                                                                                    <th scope="col">Status</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th scope="row">Height</th>
                                                                                    <td><i class="far fa-check-circle"></i></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Weight</th>
                                                                                    <td><i class="far fa-check-circle"></i></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="transaction-item">
                                                        <p class="timeline-date">May 3, 2019</p>
                                                        <div class="transaction-card-container">
                                                            <div class="tag-status done"></div>
                                                            <div class="user-details-container">
                                                                <div class="avatar-container">
                                                                    <img src="img/dashboard/avatar.png" alt="">
                                                                </div>
                                                                <div class="trans-info-prof">
                                                                    <p class="user-name">John Doe</p>
                                                                    <p class="timestamp">Yesterday</p>
                                                                </div>
                                                            </div>
                                                            <div class="transaction-card-content">
                                                                <div class="transaction-type-container">
                                                                    <div class="trans-icon">
                                                                        <img src="img/dashboard/status/user.png" alt="">
                                                                    </div>
                                                                    <span>Profile Completed</span>
                                                                </div>
                                                                <div class="transaction-content">
                                                                    <div class="icon-created">
                                                                        <span>
                                                                            <i class="fas fa-user-check"></i>
                                                                        </span>
                                                                    </div>
                                                                    <p class="text-profile-status">
                                                                        Account has been Created
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="transaction-item">
                                                        <p class="timeline-date">May 3, 2019</p>
                                                        <div class="transaction-card-container">
                                                            <div class="tag-status done"></div>
                                                            <div class="user-details-container">
                                                                <div class="avatar-container">
                                                                    <img src="img/dashboard/avatar.png" alt="">
                                                                </div>
                                                                <div class="trans-info-prof">
                                                                    <p class="user-name">John Doe</p>
                                                                    <p class="timestamp">Yesterday</p>
                                                                </div>
                                                            </div>
                                                            <div class="transaction-card-content">
                                                                <div class="transaction-type-container">
                                                                    <div class="trans-icon">
                                                                        <img src="img/dashboard/status/user.png" alt="">
                                                                    </div>
                                                                    <span>Profile Completed</span>
                                                                </div>
                                                                <div class="transaction-content">
                                                                    <div class="icon-created">
                                                                        <span>
                                                                            <i class="fas fa-birthday-cake"></i>
                                                                        </span>
                                                                    </div>
                                                                    <p class="text-profile-status">
                                                                        Born on September 19, 1993
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">PROFILE CONTENT</div>
                                        <div class="tab-pane fade" id="pills-consultation" role="tabpanel" aria-labelledby="pills-consultation-tab">
                                            <div class="transaction-timeline">
                                                <div class="no-post-container">
                                                    <p>No Available Timeline Post.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- widgets -->
                            <div class="col-lg-2 col-md-2">
                                <div class="widget-container">
                                    <div class="health-container-widget">
                                        <div class="health-outline-wrapper">
                                            <div class='widget-title-container'>
                                                <div class="icon-container">
                                                    <img src="{{ URL::asset('front-assets/img/dashboard/health.png')}}" alt="">
                                                </div>
                                                <p class="title-widget">
                                                    Health
                                                </p>
                                            </div>

                                            <div class="info-container">
                                                <p class="widget-data text-center">
                                                    100%
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bmi-container-widget">
                                        <div class="bmi-outline-wrapper">
                                            <div class='widget-title-container'>
                                                <div class="icon-container">
                                                    <img src="{{ URL::asset('front-assets/img/dashboard/bloodP.png')}}" alt="">
                                                </div>
                                                <p class="title-widget">
                                                    BMI
                                                </p>
                                            </div>

                                            <div class="info-container">
                                                <p class="widget-data text-center">
                                                    28
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="age-container-widget">
                                        <div class="age-outline-wrapper">
                                            <div class='widget-title-container'>
                                                <div class="icon-container">
                                                    <img src="{{ URL::asset('front-assets/img/dashboard/age.png')}}" alt="">
                                                </div>
                                                <p class="title-widget">
                                                    Age
                                                </p>
                                            </div>

                                            <div class="info-container">
                                                <p class="widget-data text-center">
                                                    32
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bloodtype-container-widget">
                                        <div class="bloodtype-outline-wrapper">
                                            <div class='widget-title-container'>
                                                <div class="icon-container">
                                                    <img src="{{ URL::asset('front-assets/img/dashboard/bloodt.png')}}" alt="">
                                                </div>
                                                <p class="title-widget">
                                                    Blood Type
                                                </p>
                                            </div>
                                            <div class="info-container">
                                                <p class="widget-data text-center">
                                                    B +
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="notification">
        <ul class="notification-list-pop">
            <li class="notification-item">
                <div class="notification--content">
                    <div class="row no-gutters">
                        <div class="col-md-3">
                            <div class="img-user-notif-container">
                                <div class="img-user-notif">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="info-notif-container">
                                <p class="info-username">
                                    Dr. John Doe
                                </p>
                                <p class="info-notif-content">
                                    Sent you a Consultation Request
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a href="#" class="btn btn-close"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div class="notification--actions clearfix">
                    <div class="row no-gutters">
                        <div class="col-6 left-container">
                            <a href="" class="left btn btn-block">Decline</a>
                        </div>
                        <div class="col-6 right-container">
                            <a href="" class="right btn btn-block">Confirm</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="{{ URL::asset('front-assets/frameworks/bootstrap-4.3.1-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Main JavaScript -->
    <script src='{{ URL::asset('front-assets/js/main.js')}}'></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(window).on("load", function() {
                $(".notification-list").mCustomScrollbar({
                    scrollbarPosition: "inside",
                    autoHideScrollbar: true,
                    theme: 'minimal-dark',
                });

                setTimeout(function() {
                    $('.notification').addClass('active-notif-container');
                    $('.notification-item').addClass('active');
                }, 3000);
            });


        });
    </script>

</body>


</html>.
