<!DOCTYPE HTML>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('front-assets/frameworks/bootstrap-4.3.1-dist/css/bootstrap.min.css')}}">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ URL::asset('front-assets/frameworks/fontawesome-free-5.8.1-web/css/all.min.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ URL::asset('front-assets/css/main.css')}}">

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    @yield('title')

</head>

<body>
    @yield('content')


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="{{ URL::asset('front-assets/frameworks/bootstrap-4.3.1-dist/js/bootstrap.min.js')}}"></script>
    <!-- Main JavaScript -->
    <script src='{{ URL::asset('front-assets/js/main.js')}}'></script>
    <script src='{{ URL::asset('front-assets/js/fireflies_canvas.js')}}'></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('[data-toggle="tooltip"]').tooltip({
                boundary: 'viewport',
                delay: {
                    "show": 500,
                    "hide": 100
                }
            });

            $('.form-group').focusin(function() {

                if (($(this).hasClass("input-error"))) {
                    console.log('sadsadsa');
                    $(this).children('div').find('span').tooltip('show', {
                        delay: {
                            "show": 500,
                            "hide": 100
                        }
                    });
                } else {
                    $(this).children('div').find('span').tooltip('show', {
                        delay: {
                            "show": 500,
                            "hide": 100
                        }
                    });
                }

            }).focusout(function() {
                $(this).children('div').find('span').tooltip('hide')
            }).blur(function() {
                $(this).children('div').find('span').tooltip('hide')
            });

            $('.form-group.input-error').children('div').find('span').on('show.bs.tooltip', function() {
                setTimeout(function() {
                    $('.tooltip').addClass('error-tooltip');
                    console.log('time');
                }, 10);
            })
        });
    </script>

</body>


</html>
