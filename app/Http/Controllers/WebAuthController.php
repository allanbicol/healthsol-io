<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\CivilStatus;
use App\Countries;
use App\BloodTypes;
use Session;
use Mail;
use DB;
use App\User;
use App\UserInfo;

class WebAuthController extends Controller
{
    function index(){
        if (!session()->has('tab')){
            session(['tab' => 'login-tab']);
        }

        return view('front-end.login');
    }

    function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        session(['tab' => 'login-tab']);

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('healthsolio')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return back()->with('error','Wrong Login Details');
        }
    }

    function logout(){
        Auth::logout();
        return redirect('/');
    }

    function register(Request $request)
    {
        session(['tab' => 'register-tab']);

        if ( $request->username ==""){
            return back()->with('errorusername','Username required');
        }
        $this->validate($request, [
            'username' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $token = $user->createToken('healthsolio')->accessToken;
        $data = [
            'token'=>$token,
        ];
        Mail::send('front-end.emails.welcome', ['data'=>$data], function ($message) use ($request) {
            $message->from('support@healthsol.io', 'Healthsol IO');
            $message->to($request->email);
            $message->subject('Healthsol Account Email Confirmation');
        });


            User::where('username',$request->username)->update(['signup_token' => $token,'step_completed'=>1]);
            return back()->with('signup-success','Successfully Sign-up,<br> Please check your email for the next steps.<br>Thank you!');

        // return response()->json(['token' => $token], 200);
    }

    function profileSetup(Request $request){
        if(isset($request->token)){
            if ($request->token !=''){
                $user = DB::table('users')->where('signup_token', $request->token)->first();
                // echo($user->id);
            }
            $civil_status = CivilStatus::orderBy('id','asc')
                        ->get();

            $countries = Countries::orderBy('country_id','asc')
                        ->get();

            $blood_types = BloodTypes::orderBy('blood_type_id','asc')
                        ->get();
            return view('front-end.profilesetup',['civil_status'=>$civil_status,'countries'=>$countries,'blood_types'=>$blood_types,'user_id'=>$user->id,'username'=>$user->username]);
        }else{
            return abort(404);
        }
    }

    function checkUserExist($id){
        $user = DB::table('users')->where('id', $id)->count();
        return $user;
    }

    function saveUserInfo(Request $request){

        $this->validate($request, [
            'firstname' => 'required',
            'middlename' => 'required',
            'lastname' => 'required',
        ]);

        $is_exist = $this->checkUserExist($request->input('user_id'));
        if ($is_exist==0){
            $userInfo = new UserInfo();
            $userInfo->user_id =  $request->input('user_id');
            $userInfo->username = $request->input('username');
            $userInfo->f_name = $request->input('firstname');
            $userInfo->m_name = $request->input('middlename');
            $userInfo->l_name = $request->input('lastname');
            $userInfo->gender = $request->input('gender');
            $userInfo->civil_status = $request->input('civilstatus');
            $userInfo->ethnicity_country_id = $request->input('ethnicity_country');
            $userInfo->dob = $request->input('dob');
            $userInfo->pob = $request->input('pob');
            $userInfo->height = $request->input('height');
            $userInfo->weight = $request->input('weight');
            $userInfo->bloodtype = $request->input('bloodtype');
            $saveUserInfo = $userInfo->save();
        }else{
            $saveUserInfo = UserInfo::where('user_id',$request->input('user_id'))->update(
                [
                    'username' => $request->input('username'),
                    'f_name' => $request->input('firstname'),
                    'm_name' => $request->input('middlename'),
                    'l_name' => $request->input('lastname'),
                    'gender' => $request->input('gender'),
                    'civil_status' => $request->input('civilstatus'),
                    'ethnicity_country_id' => $request->input('ethnicity_country'),
                    'dob' => $request->input('dob'),
                    'pob' => $request->input('pob'),
                    'height' => $request->input('height'),
                    'weight' => $request->input('weight'),
                    'bloodtype' => $request->input('bloodtype')
                ]);
        }
        

        User::where('id',$request->input('user_id'))->update(['step_completed'=>2]);

        // if($saveUserInfo){
            return redirect()->route('contact-setup');
        // }
    }

    function contactSetup(Request $request){
        return view('front-end.contactSetup');
    }
}
