<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_countries';
}
