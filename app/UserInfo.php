<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_info';

    protected $primaryKey = 'user_id';

    const CREATED_AT = null;
    const UPDATED_AT = null;
}
