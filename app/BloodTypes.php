<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BloodTypes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_blood_types';
}
